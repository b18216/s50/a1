import { useEffect, useState } from 'react';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import { Container } from 'react-bootstrap';
import AppNavBar from './components/AppNavBar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import CourseView from './pages/CourseView';
import { UserProvider } from './UserContext';
import './App.css';

function App() {
  
const [user, setUser]=useState({
  id: null,
  isAdmin: null
})

const unsetUser = ()=>{
localStorage.clear()
}

useEffect(()=>{

})
  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavBar/>
          <Container>
            <Routes>
              <Route exact path="/" element={<Home/>} />
              <Route exact path="/courses" element={<Courses/>} />
              <Route exact path="/courseView/:courseId" element={<CourseView/>} />
              <Route exact path="/register" element={<Register/>} />
              <Route exact path="/login" element={<Login/>} />
              <Route exact path="/logout" element={<Logout/>} />
              <Route exact path="*" element={<Error/>} />
            </Routes>
          </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
