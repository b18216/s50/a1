import{Container, Row, Col, Card, Button} from 'react-bootstrap';
import {useState, useEffect} from 'react'
import { useParams } from 'react-router-dom';

export default function CourseView(){
    
    const {courseId}= useParams();
    
    const [name, setName]= useState("");
    const [description, setDescription]= useState("");
    const [price, setPrice]= useState(0);    
    

    const enroll = (courseId)=>{

        fetch('http://locahost:4000/users/enroll', {
            method: 'POST',
            
        })
    }
    useEffect(()=>{

        fetch(`http://localhost:4000/courses/getSingleCourse/${courseId}`)
        .then(res => res.json())
        .then(data=>{
            setName(data.name)
            setPrice(data.price)
            setDescription(data.description)

        })
    }, [courseId])
    return(
    
        <Container className="mt-5">
            <Row>
                <Col lg={{span:6, offset:3}}>
                    <Card>
                        <Card.Body>
                            <Card.Title>{name}</Card.Title>

                            <Card.Subtitle>Description</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>
                            
                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>{price}</Card.Text>
                            
                            <Card.Subtitle>Class Schedule:</Card.Subtitle>
                            <Card.Text>8am to 5pm</Card.Text>
                            
                            <Button variant="primary">Enroll</Button>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>

    )
}