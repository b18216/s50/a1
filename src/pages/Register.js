import{Form, Button} from 'react-bootstrap';
import{useEffect, useState, useContext} from 'react';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';

export default function Register() {

  const {user, setUser} = useContext(UserContext)

  const [firstName, setFirstName]=useState('');
  const [lastName, setLastName]=useState('');
  const [mobileNo, setMobileNo]=useState('');
  const [email, setEmail]=useState('');
  const [password, setPassword]=useState('');
// const [password1, setPassword1]=useState('');
const [isActive, setIsActive]=useState(false);

useEffect(()=>{
    if(firstName!=='' && lastName !==''&& mobileNo.length === 11 && email!==''&& password!==''){
        setIsActive(true)
    }else{
        setIsActive(false)
    }
},[firstName, lastName, mobileNo,email, password,])

function registerUser(e){
    e.preventDefault();

    fetch('http://localhost:4000/users/checkEmailExists', {
      methond:'POST',
      headers:{
        'Content-Type':'application/json'
      },
      body: JSON.stringifyy({email:email})
    })
    .then(res=>res.json())
    .then(data=>{

      
    })

    setFirstName('')
    setLastName('')
    setEmail('');
    setPassword('');

}

    return (
      (user.id !== null)?
      <Navigate to ="/"/>
      :
    <>    
    <h1>Register Here:</h1>
    <Form onSubmit={e=>registerUser(e)}>

    <Form.Group controlId="firstName">
        <Form.Label>First Name:</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter First Name"
          required
          value={firstName}
          onChange={e => setFirstName(e.target.value)}
        ></Form.Control>
      </Form.Group>

      <Form.Group controlId="lastName">
        <Form.Label>Last Name:</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter Last Name"
          required
          value={lastName}
          onChange={e => setLastName(e.target.value)}
        ></Form.Control>
      </Form.Group>
      
      <Form.Group controlId="mobileNo">
        <Form.Label>Enter Mobile Number:</Form.Label>
        <Form.Control
          type="txt"
          placeholder="Mobile Number"
          required
          value={mobileNo}
          onChange={e => setMobileNo(e.target.value)}
        ></Form.Control>
      </Form.Group>

      <Form.Group controlId="userEmail">
        <Form.Label>Email Address:</Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter Email"
          required
          value={email}
          onChange={e =>setEmail(e.target.value)}
        ></Form.Control>
        <Form.Text className="text-muted"></Form.Text>
      </Form.Group>
      
      <Form.Group controlId="password">
        <Form.Label>Password:</Form.Label>
        <Form.Control
          type="password"
          placeholder="Enter Password"
          required
          value={password}
          onChange= {e=> setPassword(e.target.value)}
        ></Form.Control>
      </Form.Group>
             
        {isActive ?
      <Button className ="mt-3 mb-3" variant="success" type="submit"id="submitBtn">Register</Button>
        :
      <Button className ="mt-3 mb-3" variant="success" type="submit"id="submitBtn" disabled>Register</Button>
    }
    </Form>
</>

  );
}
