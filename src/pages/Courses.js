import { useEffect, useState } from 'react';
import CourseCards from '../components/CourseCards';

export default function Courses(){
    // const courses = coursesData.map(course=>{
    //     return(
    //             <CourseCards key= {course.id} courseProp = {course}/>
    //           )
    // });

 const [courses, setCourses]= useState([])


    useEffect(()=>{
        fetch('http://localhost:4000/courses')
        .then(res=>res.json())
        .then(data=>{
            console.log(data)
            setCourses(data.map(course =>{
                return(
                    <CourseCards key={course._id} courseProp={course}/>
                )
            }))
        })
    }, [])
return(
<div>
    <h1>Courses Available:</h1>
    {courses}
</div>
)
}