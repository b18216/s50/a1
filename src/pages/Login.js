import{Form, Button} from 'react-bootstrap';
import{useEffect, useState,useContext} from 'react';
// import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Login(props) {

const {user, setUser} = useContext(UserContext)

const [email, setEmail]=useState('');
const [password, setPassword]=useState('');
const [isActive, setIsActive]=useState(true);


function loginUser(e){

  e.preventDefault();
  fetch('http://localhost:4000/users/login', {
    method: "POST",
    headers: {
        'Content-Type' : 'application/json'
    },
    body: JSON.stringify({
        email: email,
        password: password
    })
})
.then(res => res.json())
.then(data => {
    console.log(data)
    if(typeof data.accessToken !== 'undefined'){
      localStorage.setItem('token', data.accesstoken)
      retrieveUserDetails(data.accessToken)

      Swal.fire({
          title:'Login Successful',
          icon:'success',
          text:'Welcome to the Booking App!'
      })
    }else{
      Swal.fire({
          title:'Login Failed',
          icon:'warning',
          text:'Try Again!'

      })
    }
})
  // localStorage.setItem('email', email)

  // setUser({
  //     email:localStorage.getItem('email')
  //   });

    setEmail('');
    setPassword('');
  

  }

  const retrieveUserDetails = (token)=>{

    fetch('http://localhost:4000/users/getUserDetails',{
      headers:{
        Authorization: `Bearer ${token}`
      }
    })

    .then(res =>res.json())
    .then(data=>{
      console.log(data)

      setUser({
        _id: data._id,
        isAdmin: data.isAdmin
      })
    })
  }

  useEffect(()=>{
    if(email !=='' && password !==''){
        setIsActive(true)
    }else{
        setIsActive(false)
    }
    },[email, password])

    return (

      // (user.email !== null)?
      // <Navigate to= "/courses"/>
      // :
    <>    
    <h1>Log In:</h1>
    <Form onSubmit={e=>loginUser(e)}>
      <Form.Group controlId="userEmail">
        <Form.Label>Email Address:</Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter Email"
          required
          value={email}
          onChange = {e =>setEmail(e.target.value)}
        ></Form.Control>
        <Form.Text className="text-muted"></Form.Text>
      </Form.Group>

      <Form.Group controlId="password">
        <Form.Label>Password:</Form.Label>
        <Form.Control
          type="password"
          placeholder="Enter Password"
          required
          value={password}
          onChange= {e=> setPassword(e.target.value)}
        ></Form.Control>
      </Form.Group> 

    {isActive ?
      <Button className ="mt-3 mb-3" variant="success" type="submit"id="submitBtn">Log In</Button>
        :
      <Button className ="mt-3 mb-3" variant="warning" type="submit"id="submitBtn" disabled>Log In</Button>
    }
    </Form>
    </>

  );
}
