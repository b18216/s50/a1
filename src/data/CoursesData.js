const CoursesData =[
{
    id:"wdc001",
    name:"PHP-Laravel",
    description:"Lorem ipsum dolor sit amet consectetur, adipisicing elit. Consequuntur consequatur omnis aperiam provident alias rerum dolores laboriosam ipsam voluptatum debitis!",
    price: 2500,
    onOffer: true

},
{
    id:"wdc002",
    name:"Django",
    description:"Lorem ipsum dolor sit amet consectetur, adipisicing elit. Consequuntur consequatur omnis aperiam provident alias rerum dolores laboriosam ipsam voluptatum debitis!",
    price: 1500,
    onOffer: true
},
{
    id:"wdc003",
    name:"Java-Springfoot",
    description:"Lorem ipsum dolor sit amet consectetur, adipisicing elit. Consequuntur consequatur omnis aperiam provident alias rerum dolores laboriosam ipsam voluptatum debitis!",
    price: 5500,
    onOffer: true
}
]
export default CoursesData;