import { Card} from 'react-bootstrap';
// import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

export default function CourseCards ({courseProp}){
	const {name, description, price, _id} = courseProp;
	
	return(
			<Card className="cardHighlight p-3">
				<Card.Body>
					<Card.Title><h2>{name}</h2></Card.Title>
					<Card.Text>{description}</Card.Text>
					<Card.Title>Price:</Card.Title>
					<Card.Text>{price}</Card.Text>
					<Link className=" btn btn-primary" to={`/courseView/${_id}`}>View Details</Link>
				</Card.Body>           					
			</Card>
    )}